<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\DA\CcanModel;

date_default_timezone_set("Asia/Makassar");
class CcanController extends Controller
{
    public function input_jt(){
    	return view('ccan.input_jt');
    }

    public function save_input_jt(Request $req){
    	CcanModel::save_jt($req);
    	return redirect('/');
    }

    public function input_pt1($id){
        $data = CcanModel::show_single($id);
        return view('ccan.approve_pt1', ['data' => $data]);
    }

    public function save_input_pt1(Request $req, $id){
    	if($req->select_Status == 0){
            CcanModel::save_pt1($id, 6);
        }else{
          CcanModel::save_pt1($id, 4);
        }
    }

    public function see_detail($id){
       $step = CcanModel::step($id);
       return view('detail_project', ['data_step' => $step]);
   }
}
