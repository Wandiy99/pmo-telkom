<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use DB;

class LoginController extends Controller
{
    public function loginPage()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $username  = $request->input('username');
        $password = $request->input('password');

        $result = DB::select('
          SELECT *
          FROM account
          WHERE username = ? AND password = MD5(?)
        ', [
          $username,
          $password
        ]);
        //dd($result);
        if (count($result) == 1) {
            Session::put('auth', $result[0]);
            return redirect('/');
        }else{
            return redirect()->back()->with('alerts', [
                ['type' => 'danger', 'text' => 'GAGAL LOGIN']
            ]);
        }
    }
    public function logout()
    {
        Session::forget('auth');
        return redirect('/login');
    }
}
