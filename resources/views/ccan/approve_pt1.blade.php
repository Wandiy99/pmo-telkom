@extends('layout')
@section('title', 'Approval PT1')
@section('content')
@section('css')
<style type="text/css">
#map {
	height: 100%;
}
.row-eq-height {
	display: -webkit-box;
	display: -webkit-flex;
	display: -ms-flexbox;
	display: flex;	
}
</style>
@endsection
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="row-eq-height">
		<div class="col-md-4">
			<div class="form-group form-group-sm">
				<label for="ticares_id" class="col-md-4 control-label">Ticares ID :</label>
				<div class="col-md-8">
					<label>{{ $data->ticares_id ?: '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="tgl_input" class="col-md-4 control-label">Tanggal Input :</label>
				<div class="col-md-8">
					<label>{{ $data->input_tgl ? date('Y-m-d', $data->input_tgl) : '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="Customer" class="col-md-4 control-label">Customer :</label>
				<div class="col-md-8">
					<label>{{ $data->customer ?: '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="Customer" class="col-md-4 control-label">Alamat :</label>
				<div class="col-md-8">
					<label>{{ $data->alamat ?: '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="pic" class="col-md-4 control-label">PIC :</label>
				<div class="col-md-8">
					<label>{{ $data->pic ?: '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="koor" class="col-md-4 control-label">Koordinat :</label>
				<div class="col-md-8">
					<label>{{ $data->koor ?: '' }}</label>
				</div>
				<div id="map"></div>
			</div>
			<div class="form-group form-group-sm">
				<label for="type_service" class="col-md-4 control-label">Tipe Layanan :</label>
				<div class="col-md-8">
					<label>{{ $data->layanan ?: '' }}</label>
				</div>

			</div>
			<div class="form-group form-group-sm">
				<label for="sto_code" class="col-md-4 control-label">Kode STO :</label>
				<div class="col-md-8">
					<label>{{ $data->sto ?: '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="bast" class="col-md-4 control-label">Data Nota :</label>
				<div class="col-md-8">
					<label>{{ $data->sto ?: '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="nota_dinas" class="col-md-4 control-label">Nota Dinas :</label>
				<div class="col-md-8">
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="status_mitra" class="col-md-4 control-label">Status Mitra :</label>
				<div class="col-md-8">
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="status_selection" class="col-md-4 control-label">Status </label>
				<div class="col-md-8">
					<select class="form-control" id="select_Status" name="select_Status">
						<option value="0">Close JT</option>
						<option value="1">Kirim Ulang Ke Gs</option>
					</select> 
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="ket" class="col-md-4 control-label">Keterangan</label>
				<div class="col-md-8">
					<textarea rows="3" class="form-control" id="ket" name="ket"></textarea>
				</div>
			</div>
			<div class="form-group form-group-sm" id="detach_img">
				<div class="row text-center input-photos">
					<?php
					$number = 1;
					clearstatcache();
					?>
					{{-- <img src="/images/placeholder.gif" alt="" class="img_src" id="img-"/> --}}
					<br />
					<p id="title"></p>
					<?php
					$number++;
					?>
				</div>
			</div>
		</div>
		<div class="sticy_bitchis col-md-8">
			<div class="row main_log">
				<div id="log"></div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-offset-2 ">
			<button type="submit" class="btn btn-block"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
		</div>
	</div>
</form>
@endsection
@section('js')
<script type="text/javascript">
	$(function() {
		$("#select_Status").val("");
		$.ajax({
			type: "GET",
			url: "/log",
			success: function(data){
				$('#log').html(data);
				$('thead, tbody tr').css({
					'display':'table',
					'width':'100%',
					'table-layout':'fixed'
				});
				$('thead').css({
					'width': 'calc( 100% - 1em )'
				});
			}
		});

		$(window).scroll(function() {
			var windowTop = $(window).scrollTop();
			if (0 < windowTop && $(".sticy_bitchis").height() > windowTop) {
				$('.main_log').css({
					'position': '-webkit-sticky',
					'position': 'sticky',
					'top': '44px'
				});
			} else {
				$('.main_log').css({
					'position': 'relative',
					'top': '0px'
				});
			}
		});

	});
</script>

@endsection