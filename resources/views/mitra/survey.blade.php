@extends('layout')
@section('title', 'Survey')
@section('content')
@section('css')
<style type="text/css">
#map {
	height: 100%;
}
.row-eq-height {
	display: -webkit-box;
	display: -webkit-flex;
	display: -ms-flexbox;
	display: flex;	
}
</style>
@endsection
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="row row-eq-height">
		<div class="col-md-6">
			<div class="form-group form-group-sm">
				<label for="ticares_id" class="col-md-4 control-label">Ticares ID :</label>
				<div class="col-md-8">
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="tgl_input" class="col-md-4 control-label">Tanggal Input :</label>
				<div class="col-md-8">
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="Customer" class="col-md-4 control-label">Customer :</label>
				<div class="col-md-8">
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="Customer" class="col-md-4 control-label">Alamat :</label>
				<div class="col-md-8">
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="pic" class="col-md-4 control-label">PIC :</label>
				<div class="col-md-8">
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="koor" class="col-md-4 control-label">Koordinat :</label>
				<div class="col-md-8">
				</div>
				<div id="map"></div>
			</div>
			<div class="form-group form-group-sm">
				<label for="type_service" class="col-md-4 control-label">Tipe Layanan :</label>
				<div class="col-md-8">
				</div>

			</div>
			<div class="form-group form-group-sm">
				<label for="sto_code" class="col-md-4 control-label">Kode STO :</label>
				<div class="col-md-8">
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="bast" class="col-md-4 control-label">Lampiran Data Nota :</label>
				<div class="col-md-8">
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="nota_dinas" class="col-md-4 control-label">Lampiran Nota Dinas :</label>
				<div class="col-md-8">
				</div>
			</div>
		</div>
		<div class="sticy_bitchis col-md-6">
			<div class="content_gs" >
				<div class="form-group form-group-sm">
					<label for="status_mitra" class="col-md-4 control-label">Status</label>
					<div class="col-md-8">
						<select class="form-control" id="select_Status">
							<option value="pt1">Pt1</option>
							<option value="pt23">Pt2/Pt3</option>
							<option value="ts">Tidak Sanggup</option>
						</select> 
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="ket" class="col-md-4 control-label">Keterangan</label>
					<div class="col-md-8">
						<textarea rows="3" class="form-control" id="ket"></textarea>
					</div>
				</div>
				<div class="form-group form-group-sm container_rab">
					<div id="upload_rab">
						<label for="ket" class="col-md-4 control-label">Upload RAB</label>
						<div class="col-md-8">
							<input type="file" name="nota_dinas" id="nota_dinas" value="">
						</div>
					</div>
				</div>
				<div class="form-group form-group-sm" id="detach_img">
					<div class="row text-center input-photos">
						<?php
						$number = 1;
						clearstatcache();
						?>
						<img src="/images/placeholder.gif" alt="" class="img_src" id="img-"/>
						<br />
						<input type="file" class="hidden" id="photo" name="" accept="image/jpeg" />
						<p id="title"></p>
						<button style="text-align: center;" type="button" class="btn"><span class="btn-sm btn-info fa fa-camera"></span>
						</button>
						<?php
						$number++;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-offset-2 ">
			<button type="submit" class="btn btn-block"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
		</div>
	</div>

	<div class="row">
		<div id="log"></div>
	</div>
</form>
@endsection
@section('js')
<script type="text/javascript">
	$(function() {

		/* ini mencari gambar, jangan dihapus
		// function testImage(url) {
		// 	var tester = new Image();
		// 	tester.addEventListener('load', imageFound);
		// 	tester.addEventListener('error', imageNotFound);
		// 	tester.src = url;
		// }

		// function imageFound() {
		// 	alert('That image is found and loaded');
		// }

		// function imageNotFound() {
		// 	alert('That image was not found.');
		// }

		// testImage("/images/placeholder1.gif")
		*/
		$("#select_Status").val("pt1");
		var selected_status = {
			'pt1' : {
				'phpto' : 'ODP',
				'file' : null
			},
			'pt23' : {
				'phpto' : 'KML',
				'file' : 'RAB'
			}
		};
		var photo_result_select = function(data){
			$(".img_src").attr('id', data);
			$("#photo").attr('name', data);
			$("#title").text(data);
		};

		var value1 = $("#select_Status").val();
		var img_hid;
		var rab = $('#upload_rab').detach();
		$('#select_Status').change(function(){
			console.log(img_hid);
			if ($(this).val() !== 'ts'){
				if(img_hid){
					img_hid.appendTo( "#detach_img" );
					img_hid = null;
				}
				if(selected_status[$(this).val()].file != null){
					rab.appendTo( ".container_rab" );
					rab = null;
				}else{
					rab = $('#upload_rab').detach();
				}
				photo_result_select(selected_status[$(this).val()].phpto);
			}else{
				if(rab === null){
					rab = $('#upload_rab').detach();
				}
				if(rab === null){
					img_hid = $( ".input-photos" ).detach();
				}
			}
		});
		photo_result_select(selected_status[value1].phpto);
		$('input[type=file]').change(function() {
			console.log(this.name);
			var inputEl = this;
			if (inputEl.files && inputEl.files[0]) {
				$(inputEl).parent().find('input[type=text]').val(1);
				var reader = new FileReader();
				reader.onload = function(e) {
					$(inputEl).parent().find('img').attr('src', e.target.result);

				}
				reader.readAsDataURL(inputEl.files[0]);
			}
		});

		$('.input-photos').on('click', 'button', function() {
			$(this).parent().find('input[type=file]').click();
		});
		$.ajax({
			type: "GET",
			url: "/log",
			success: function(data){
				$('#log').html(data);
			}
		});

		$(window).scroll(function() {
			var windowTop = $(window).scrollTop() + 50;
			// console.log($('.content_gs').offset().top);
			// console.log('windows top '+windowTop);
			// console.log($(".sticy_bitchis").height());
			// console.log($(".sticy_bitchis").offset().top)
			// console.log($(".content_gs").height());
			// console.log('content gs '+$('.content_gs').offset().top);
			// console.log($(".sticy_bitchis").height() + $(".sticy_bitchis").offset().top - $(".content_gs").height());
			if ($('.content_gs').offset().top < windowTop) {
				// console.log('benar');
			// 	$('.content_gs').css({
			// 		'position': '-webkit-sticky',
			// 		'position': 'sticky',
			// 		'top': '44px'
			// 	});
		} else {
			// console.log('salah');
			// 	$('.content_gs').css({
			// 		'position': 'relative',
			// 		'top': '0px'
			// 	});
			// 	
		}
	});
	});
</script>

@endsection