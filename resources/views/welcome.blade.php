@extends('layout')
@section('title', 'Dashboard')
@section('content')
@section('css')
<style type="text/css">
.wizard {
	margin: 20px auto;
	background: #fff;
}

.wizard .nav-tabs {
	position: relative;
	max-width: 100%;
	max-height: 100%;
	height: inherit !important;
	border-bottom-color: #e0e0e0;
}

.wizard > div.wizard-inner {
	position: relative;
}

.connecting-line {
	height: 2px;
	background: #e0e0e0;
	position: absolute;
	width: 100%;
	margin: 6px auto;
	left: 0;
	right: 0;
	top: 50%;
	z-index: 1;
}
.nav-tabs > li {

	float: left;
	margin-bottom: -7px;

}
.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
	color: #555555;
	cursor: default;
	border: 0;
	border-bottom-color: transparent;
}

span.round-tab {
	width: 100%;
	height: 100%;
	line-height: 48px;
	display: inline-block;
	border-radius: 100px;
	background: #fff;
	border: 2px solid #e0e0e0; /* ganti warna */
	z-index: 2;
	position: absolute;
	left: 0;
	text-align: center;
	font-size: 20px;
}
span.round-tab i{
	color:#555555;
}
.wizard li.active span.round-tab {
	background: #fff;
	border: 2px solid #5bc0de;

}
.wizard li.active span.round-tab i{
	color: #5bc0de; /*this is color round*/
}

span.round-tab:hover {
	color: #333;
	border: 2px solid #333;
}

.wizard .nav-tabs > li {
	width: auto;
}

.nav > li + li {
	margin-left: 4.48%
}

.wizard li:after {
	content: " ";
	position: absolute;
	left: 30%;
	opacity: 0;
	margin: 0 auto;
	bottom: 0px;
	border: 5px solid transparent;
	border-bottom-color: #5bc0de;
	transition: 0.1s ease-in-out;
}

.wizard li.active:after {
	content: " ";
	position: absolute;
	left: 30%;
	opacity: 1;
	margin: 6px auto;
	bottom: 0px;
	border: 10px solid transparent;
	border-bottom-color: #5bc0de;
}

.wizard .nav-tabs > li a {
	width: 55px;
	height: 55px;
	margin: 20px auto;
	border-radius: 100%;
	padding: 0;
}

.wizard .nav-tabs > li a:hover {
	background: transparent;
}

.wizard .tab-pane {
	position: relative;
	padding-top: 20px;
}

.wizard h3 {
	margin-top: 0;
}

@media( max-width : 585px ) {

	.wizard {
		width: 90%;
		height: auto !important;
	}

	span.round-tab {
		font-size: 16px;
		width: 100%;
		height: 100%;
		line-height: 100%;
	}

	.wizard .nav-tabs > li a {
		width: 55px;
		height: 55px;
		line-height: 100%;
	}

	.wizard li.active:after {
		content: " ";
		position: absolute;
		left: 35%;
	}
}
</style>
@endsection
<div class="container-fluid">
	<div class="row shrink">
		<section>
			<div class="wizard">
				<div class="wizard-inner">
					<div class="connecting-line"></div>
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
								<span class="round-tab">
									<i class="fa fa-question"></i>
								</span>
							</a>
						</li>

						<li role="presentation" class="disabled">
							<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
								<span class="round-tab">
									<i class="fa fa-question"></i>
								</span>
							</a>
						</li>
						<li role="presentation" class="disabled">
							<a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
								<span class="round-tab">
									<i class="fa fa-question"></i>
								</span>
							</a>
						</li>

						<li role="presentation" class="disabled">
							<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
								<span class="round-tab">
									<i class="fa fa-question"></i>
								</span>
							</a>
						</li>

						<li role="presentation" class="active">
							<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
								<span class="round-tab">
									<i class="fa fa-question"></i>
								</span>
							</a>
						</li>

						<li role="presentation" class="disabled">
							<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
								<span class="round-tab">
									<i class="fa fa-question"></i>
								</span>
							</a>
						</li>
						<li role="presentation" class="disabled">
							<a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
								<span class="round-tab">
									<i class="fa fa-question"></i>
								</span>
							</a>
						</li>

						<li role="presentation" class="disabled">
							<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
								<span class="round-tab">
									<i class="fa fa-question"></i>
								</span>
							</a>
						</li>
						<li role="presentation" class="disabled">
							<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
								<span class="round-tab">
									<i class="fa fa-question"></i>
								</span>
							</a>
						</li>
						<li role="presentation" class="disabled">
							<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
								<span class="round-tab">
									<i class="fa fa-question"></i>
								</span>
							</a>
						</li>
						<li role="presentation" class="disabled">
							<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
								<span class="round-tab">
									<i class="fa fa-question"></i>
								</span>
							</a>
						</li>
					</ul>
				</div>

				<form role="form">
					<div class="tab-content">
						<div class="tab-pane active" role="tabpanel" id="step1">
							<h3>Step 1</h3>
							<p>This is step 1</p>
							<p>This is step 1</p>							
						</div>
						<div class="tab-pane" role="tabpanel" id="step2">
							<h3>Step 2</h3>
							<p>This is step 2</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	$(function() {
		    //Initialize tooltips
		    $('.nav-tabs > li a[title]').tooltip();

		    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

		    	var $target = $(e.target);
		    	
		    	if ($target.parent().hasClass('disabled')) {
		    		return false;
		    	}
		    });
		});

	</script>

	@endsection