<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', 'LoginController@loginPage');
Route::post('/login', 'LoginController@login');
Route::group(['middleware' => 'tomman.auth'], function () {
	Route::get('/', function () {
		return view('welcome');
	});
	Route::get('/detail/{id}', 'CcanController@see_detail');
	Route::get('/logout', 'LoginController@logout');
	Route::get('/log', function () {
		return view('log');
	});
	Route::get('/approve', function () {
		return view('egbiz.approve_kelayakan');
	});
	Route::prefix('ccan')->group(function () {
		Route::get('input','CcanController@input_jt');
		Route::post('input','CcanController@save_input_jt');
		Route::get('pt1/approve/{id}', 'CcanController@input_pt1');
		Route::post('pt1/approve/{id}', 'CcanController@save_input_pt1');
	});
	Route::prefix('op')->group(function () {
		Route::get('analys', function () {
			return view('optima.analisa_kelayakan');
		});
		Route::get('nde/{id}','OptimaController@nde');
		Route::post('nde/{id}','OptimaController@save_nde');
		Route::get('ba_ut', function () {
			return view('optima.ba_ut');
		});
		Route::get('rekon', function () {
			return view('optima.rekon');
		});
	});
	Route::prefix('gs')->group(function () {
		Route::get('ba_st', function () {
			return view('gs.ba_st');
		});
		Route::get('mitraselect', function () {
			return view('gs.mitra_select');
		});
	});
	Route::prefix('mitra')->group(function () {
		Route::get('survey', function () {
			return view('mitra.survey');
		});
		Route::get('completion_work', function () {
			return view('mitra.penyelesaian_pekerjaan');
		});
	});
});